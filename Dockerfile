From docker.io/alpine:latest
RUN  mkdir /opt/server/
COPY * /opt/server/
RUN go build -o myserver
ENTRYPOINT /opt/server/myserver